const Product = require('../model/productModel')
const mongoose = require('mongoose')

const getProduct = async (req, res) => {
    const product = await Product.find({}).sort({ createdAt: -1 })
    res.status(200).json(product)
}

const getSingleProduct = async (req, res) => {

    const { id } = req.params
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(404).json({ error: "No such product" })
    }

    const product = await Product.findById(id)
    if (!product) {
        return res.status(404).json({ error: "No such product" })
    }
    res.status(200).json(product)
}

const postProduct = async (req, res) => {
    const { name, desc, price, quantity, merchant_id, discount_id, category_id, review_id } = req.body
    try {
        const product = await Product.create({ name, desc, price, quantity, merchant_id, discount_id, category_id, review_id })
        res.status((200)).json(product)
    }
    catch (error) {
        res.status((404).json({ error: error.message }))
    }
}

const deleteProduct = async (req, res) => {
    const { id } = req.params
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(404).json({ error: "No such product" })
    }
    const product = await Product.findOneAndDelete({ _id: id })
    if (!product) {
        return res.status(404).json({ error: "No such product" })
    }

    res.status(200).json(product)
}

const updateProduct = async (req, res) => {
    const { id } = req.params
    if (!mongoose.Types.ObjectId(id)) {
        return res.status(404), json({ error: 'No such product' })
    }
    const product = await Product.findOneAndUpdate({ _id: id }, {
        ...req.body
    })

    if (!product) {
        return res.status(404), json({ error: 'No such product' })
    }

    res.status(200).json(product)
}

module.exports ={
    getProduct,
    getSingleProduct,
    postProduct,
    deleteProduct,
    updateProduct
}