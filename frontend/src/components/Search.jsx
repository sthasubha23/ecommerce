import React from 'react'

export default function Search() {
    return (
        <div className="flex items-center justify-center bg-slate-200 rounded-3xl ">
            <input type="text" placeholder="Search in Bechdiyo" className="input w-full max-w-xs rounded-3xl bg-slate-200 " />
            <button type="submit" className="btn btn-sm rounded-2xl mx-3">
                Search
                <span className="material-symbols-outlined">
                    search
                </span>
            </button>
        </div>
    )
}
