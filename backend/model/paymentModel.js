const { default: mongoose, model } = require("mongoose");
const Schema = mongoose.Schema

const paymentSchema = new Schema(
    {
        order_id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Order',
        },
        status: {
            type: Boolean,
            require: true
        },
        amount: {
            type: String,
            require: true
        }
    },
    { timestamps: true }
)


module.exports = mongoose.model('Payment', paymentSchema)
