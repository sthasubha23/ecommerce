import React from 'react';
import Card from '../components/Card';
import AddProduct from '../forms/AddProduct';

const categories = [
  "Women's Fashion",
  "Health & Beauty",
  "Men's Fashion",
  "Watches & Accessories",
  "Electronic Devices",
  "TV & Home Appliances",
  "Electronic Accessories",
  "Groceries & Pets",
  "Babies & Toys",
  "Home & Lifestyle",
  "Sports & Outdoor",
  "Motors, Tools & DIY",
];
const cards = [{
  name: 'Camera',
  img: 'https://images.pexels.com/photos/51383/photo-camera-subject-photographer-51383.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
  merchant: 'WholeSale',
  price: '30000',
  discount: '10%',
  desc: 'hello welcome to the new camera world this is great camera you got to buy it',
  categories: 'camera'
}, {
  name: 'Camera',
  img: 'https://images.pexels.com/photos/51383/photo-camera-subject-photographer-51383.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
  merchant: 'WholeSale',
  price: '30000',
  discount: '10%',
  desc: 'hello welcome to the new camera world this is great camera you got to buy it',
  categories: 'camera'
}, {
  name: 'Camera',
  img: 'https://images.pexels.com/photos/51383/photo-camera-subject-photographer-51383.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
  merchant: 'WholeSale',
  price: '30000',
  discount: '10%',
  desc: 'hello welcome to the new camera world this is great camera you got to buy it',
  categories: 'camera'
}, {
  name: 'Camera',
  img: 'https://images.pexels.com/photos/51383/photo-camera-subject-photographer-51383.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
  merchant: 'WholeSale',
  price: '30000',
  discount: '10%',
  desc: 'hello welcome to the new camera world this is great camera you got to buy it',
  categories: 'camera'
}, {
  name: 'Camera',
  img: 'https://images.pexels.com/photos/51383/photo-camera-subject-photographer-51383.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
  merchant: 'WholeSale',
  price: '30000',
  discount: '10%',
  desc: 'hello welcome to the new camera world this is great camera you got to buy it',
  categories: 'camera'
}, {
  name: 'Camera',
  img: 'https://images.pexels.com/photos/51383/photo-camera-subject-photographer-51383.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
  merchant: 'WholeSale',
  price: '30000',
  discount: '10%',
  desc: 'hello welcome to the new camera world this is great camera you got to buy it',
  categories: 'camera'
}, {
  name: 'Camera',
  img: 'https://images.pexels.com/photos/51383/photo-camera-subject-photographer-51383.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
  merchant: 'WholeSale',
  price: '30000',
  discount: '10%',
  desc: 'hello welcome to the new camera world this is great camera you got to buy it',
  categories: 'camera'
}, {
  name: 'Camera',
  img: 'https://images.pexels.com/photos/51383/photo-camera-subject-photographer-51383.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
  merchant: 'WholeSale',
  price: '30000',
  discount: '10%',
  desc: 'hello welcome to the new camera world this is great camera you got to buy it',
  categories: 'camera'
}, {
  name: 'Camera',
  img: 'https://images.pexels.com/photos/51383/photo-camera-subject-photographer-51383.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
  merchant: 'WholeSale',
  price: '30000',
  discount: '10%',
  desc: 'hello welcome to the new camera world this is great camera you got to buy it',
  categories: 'camera'
},
]
const Home = () => (
  <div className="flex flex-col bg-neutral-100">
    <div className="container mx-auto flex">
      {/* Left Column */}
      <div className="w-3/12 p-4 h-auto">
        <div className="bg-neutral-50 p-6 rounded shadow">
          <ul className="menu menu-sm bg-white w-auto rounded-box">
            {categories && categories.map((item, index) => (
              <li key={index} ><a className='text-gray-500'>{item}</a></li>
            ))}
          </ul>
        </div>
      </div>

      {/* Right Column */}
      <div className="w-9/12 p-4 h-auto">
        <div className="bg-white p-6 rounded shadow">
          <div className="carousel w-full">
            <div id="slide1" className="carousel-item relative w-full">
              <img src="https://daisyui.com/images/stock/photo-1625726411847-8cbb60cc71e6.jpg" className="w-full" />
              <div className="absolute flex justify-between transform -translate-y-1/2 left-5 right-5 top-1/2">
                <a href="#slide4" className="btn btn-circle">❮</a>
                <a href="#slide2" className="btn btn-circle">❯</a>
              </div>
            </div>
            <div id="slide2" className="carousel-item relative w-full">
              <img src="https://daisyui.com/images/stock/photo-1609621838510-5ad474b7d25d.jpg" className="w-full" />
              <div className="absolute flex justify-between transform -translate-y-1/2 left-5 right-5 top-1/2">
                <a href="#slide1" className="btn btn-circle">❮</a>
                <a href="#slide3" className="btn btn-circle">❯</a>
              </div>
            </div>
            <div id="slide3" className="carousel-item relative w-full">
              <img src="https://daisyui.com/images/stock/photo-1414694762283-acccc27bca85.jpg" className="w-full" />
              <div className="absolute flex justify-between transform -translate-y-1/2 left-5 right-5 top-1/2">
                <a href="#slide2" className="btn btn-circle">❮</a>
                <a href="#slide4" className="btn btn-circle">❯</a>
              </div>
            </div>
            <div id="slide4" className="carousel-item relative w-full">
              <img src="https://daisyui.com/images/stock/photo-1665553365602-b2fb8e5d1707.jpg" className="w-full" />
              <div className="absolute flex justify-between transform -translate-y-1/2 left-5 right-5 top-1/2">
                <a href="#slide3" className="btn btn-circle">❮</a>
                <a href="#slide1" className="btn btn-circle">❯</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div className='container mx-auto flex'>
      <div className="w-9/12 p-4 h-auto">
        <Card details={cards} />
      </div>
      <div className="w-3/12 p-4 h-auto">
        <AddProduct/>
      </div>
    </div>
  </div>
);

export default Home;
