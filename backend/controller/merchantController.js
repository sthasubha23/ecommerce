const merchant = require('../model/merchantModel')
const mongoose = require('mongoose')

const getMerchant = async (req, res) => {
    const merchant = await merchant.find({}).sort({ createdAt: -1 })
    res.status(200).json(merchant)
}

const getSingleMerchant = async (req, res) => {

    const { id } = req.params
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(404).json({ error: "No such merchant" })
    }

    const merchant = await merchant.findById(id)
    if (!merchant) {
        return res.status(404).json({ error: "No such merchant" })
    }
    res.status(200).json(merchant)
}

const postMerchant = async (req, res) => {
    const { name, product_id } = req.body
    try {
        const merchant = await merchant.create({ name, product_id })
        res.status((200)).json(merchant)
    }
    catch (error) {
        res.status((404).json({ error: error.message }))
    }
}

const deleteMerchant = async (req, res) => {
    const { id } = req.params
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(404).json({ error: "No such merchant" })
    }
    const merchant = await merchant.findOneAndDelete({ _id: id })
    if (!merchant) {
        return res.status(404).json({ error: "No such merchant" })
    }

    res.status(200).json(merchant)
}

const updateMerchant = async (req, res) => {
    const { id } = req.params
    if (!mongoose.Types.ObjectId(id)) {
        return res.status(404), json({ error: 'No such merchant' })
    }
    const merchant = await merchant.findOneAndUpdate({ _id: id }, {
        ...req.body
    })

    if (!merchant) {
        return res.status(404), json({ error: 'No such merchant' })
    }

    res.status(200).json(merchant)
}

module.exports = {
    getMerchant,
    getSingleMerchant,
    postMerchant,
    deleteMerchant,
    updateMerchant
}