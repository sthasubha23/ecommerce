//initialization and Import
require('dotenv').config()
const mongoose = require('mongoose')
const express = require('express')
const cors = require('cors')

const productRoutes = require('./routes/productRouter') 
const userRoutes = require('./routes/userRouter') 
const app = express()


app.use(express.json())
//cors
const allowedOrigins = ['http://localhost:3000'];

app.use(
  cors({
    origin: (origin, callback) => {
      // Check if the request origin is allowed
      if (!origin || allowedOrigins.includes(origin)) {
        callback(null, true);
      } else {
        callback(new Error('Not allowed by CORS'));
      }
    },
  })
);

//middleware
app.use((req,res,next)=>{
    console.log(req.path,res.method)
    next()
})

//route path
app.use('/api/v1/product',productRoutes)
app.use('/api/v1/user',userRoutes)

//db connection
mongoose.connect(process.env.MONG_URI)
.then(() =>{
    app.listen(process.env.PORT,()=>{
        console.log(`CONNECTED TO ${process.env.PORT}`)
    })
})
.catch((error)=>{
    console.log(error)
})
