const User = require('../model/userModel')
const jwt = require('jsonwebtoken')

//creating jwttoken
const createToken = (id) =>{
  return jwt.sign({id},process.env.TOKEN_SECRET,{expiresIn:'3d'})
}

// login a user
const loginUser = async (req, res) => {
  const {email,number,password}= req.body
  try{
    const user = await User.login(email,number,password)
    const token = await createToken(user.id)
    res.status(200).json({user,token})
  }
  catch(error){
    res.status(400).json({error:error.message})
  }
}

// signup a user
const signupUser = async (req, res) => {
  const {email, password, username, address, number, payment_id, order_id, review_id} = req.body

  try {
    const user = await User.signup(email, password, username, address, number, payment_id, order_id, review_id)
    const token =  createToken(user.id)
    res.status(200).json({email, token})
  } catch (error) {
    res.status(400).json({error: error.message})
  }
}




module.exports = { signupUser, loginUser }