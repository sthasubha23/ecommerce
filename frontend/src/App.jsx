import { BrowserRouter, Route, Routes } from 'react-router-dom'

import Navbar from './components/Navbar'
import Home from './pages/Home'
import SignUp from './pages/SignUp'
// import './App.css'

function App() {
  return (
    <div className='App'>
      <BrowserRouter>
        <Navbar />
        <div className='pages'>
          <Routes>
            <Route
              path='/'
              element={<Home/>}
            />
            <Route
              path='/signup'
              element={<SignUp/>}
            />
          </Routes>
        </div>
      </BrowserRouter>
    </div>
  )
}

export default App
