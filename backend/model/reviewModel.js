const { default: mongoose } = require("mongoose");
const Schema = mongoose.Schema

const userModel = new Schema(
    {
        rating : {
            type: Number,
            require: true
        },
        comment: {
            type: String,
            require: true
        },
        user_id : {
            type : mongoose.Schema.Types.ObjectId,
            ref: 'User',
        }
    },
    { timestamps: true }
)

module.exports = mongoose.model('User', userModel)