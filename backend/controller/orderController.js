const category = require('../model/categoryModel')
const mongoose = require('mongoose')

const getCategory = async (req, res) => {
    const category = await category.find({}).sort({ createdAt: -1 })
    res.status(200).json(category)
}

const getSingleCategory = async (req, res) => {

    const { id } = req.params
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(404).json({ error: "No such category" })
    }

    const category = await category.findById(id)
    if (!category) {
        return res.status(404).json({ error: "No such category" })
    }
    res.status(200).json(category)
}

const postCategory = async (req, res) => {
    const { product_id, user_id, quantity, amount } = req.body
    try {
        const category = await category.create({ product_id, user_id, quantity, amount })
        res.status((200)).json(category)
    }
    catch (error) {
        res.status((404).json({ error: error.message }))
    }
}

const deletecategory = async (req, res) => {
    const { id } = req.params
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(404).json({ error: "No such category" })
    }
    const category = await category.findOneAndDelete({ _id: id })
    if (!category) {
        return res.status(404).json({ error: "No such category" })
    }

    res.status(200).json(category)
}

const updatecategory = async (req, res) => {
    const { id } = req.params
    if (!mongoose.Types.ObjectId(id)) {
        return res.status(404), json({ error: 'No such category' })
    }
    const category = await category.findOneAndUpdate({ _id: id }, {
        ...req.body
    })

    if (!category) {
        return res.status(404), json({ error: 'No such category' })
    }

    res.status(200).json(category)
}

module.exports = {
    getCategory,
    getSingleCategory,
    postCategory,
    deletecategory,
    updatecategory
}