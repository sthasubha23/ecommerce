import React, { useState } from "react";
import { Link } from "react-router-dom";
import Search from "./Search";
import logo from '/img/Bechdiyo.jpg'

const Navbar = () => {
  

  return (
    <header className="navbar bg-orange-500 shadow-lg">
      <div className="container flex flex-wrap items-center px-4 py-3">
        <div className="navbar-brand mr-auto">
          <Link to="/">
            <img src={logo} alt="Bechdiyo" className='rounded-full' style={{ width: 'auto', height: '50px' }}/>
          </Link>
        </div>
        <div className="navbar-menu hidden md:flex">
          <Search />
          <div className="navbar-right flex items-center gap-2 ml-2">

            <div className="navbar-signup">
              <Link to="/signup" className="btn btn-md bg-slate-200 border-0 rounded-3xl">
                <span className="material-symbols-outlined">
                  account_circle
                </span>
                Sign Up
              </Link>
            </div>
            <div className="navbar-login">
              <Link to="/login" className="btn btn-md bg-slate-200 border-0 rounded-3xl">
                <span className="material-symbols-outlined">
                  login
                </span>
                Login
              </Link>
            </div>

            <div className="navbar-cart ml-2 ">
              <Link to="/cart">
                <button className="btn btn-circle bg-slate-200 border-0">
                  <span className="material-symbols-outlined">
                    shopping_cart
                  </span>
                </button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Navbar;
