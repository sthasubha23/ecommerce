const mongoose = require('mongoose')
const bcrypt = require('bcrypt')
const validator = require('validator')

const Schema = mongoose.Schema

const userModel = new Schema({
    username: {
        type: String,
        require: true,
    },
    password: {
        type: String,
        require: true
    },
    number: {
        type: Number,
        require: true,
        unique: true
    },
    address: {
        type: String,
        require: true
    },
    email: {
        type: String,
        require: true,
        unique: true
    },
    payment_id: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Payment',
    }],
    order_id: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Order',
    }],
    review_id: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Review',
    }]
},
    { timestamps: true }
)

userModel.statics.signup = async function (email, password, username, address, number, payment_id, order_id, review_id) {
    
    if(!email || !password || !username || !number || !address){
        throw Error("All Field must be filled")
    }

    if(!validator.isEmail(email)){
        throw Error("Please provide Valid Email")
    }

    if(!validator.isStrongPassword(password)){
        throw Error("Given Password is not strong")
    }
    
    const exists = await this.findOne({ email })

    if (exists) {
        throw Error('Email already in use')
    }

    const salt = await bcrypt.genSalt(10)
    const hash = await bcrypt.hash(password, salt)

    const user = await this.create({ email, password: hash, username, address, number, payment_id, order_id, review_id })

    return user
}

userModel.statics.login = async function(email,number,password){
    if((!email || !number) || !password ){
        return("All field must be filled")
    }
    const user = await this.findOne({email,number})
    if(!user){
        return ("Invaldi Email")
    }

    const match = await bcrypt.compare(password,user.password)

    if(!match){
        return("Invalid Password")
    }

    return user
}

module.exports = mongoose.model('User', userModel)