import React from 'react';

export default function Card({ details }) {
    return (
        <div className="flex flex-wrap">
            {details.map((product, index) => (
                <div key={index} className='bg-inherit p-2 w-1/5 '>
                    <div className="card card-compact p-4 bg-base-100 shadow-sm">
                        <figure><img src={product.img} alt={product.name} className="w-full h-auto" /></figure>
                        <div className="card-body">
                            <h2 className="card-title">{product.name}</h2>
                            <div className='flex gap-1'>
                            <div className="badge badge-ghost p-2 badge-xs">{product.categories}</div>
                            <div className="badge badge-ghost p-2 badge-xs">{product.merchant}</div>
                            </div>
                            <p>{product.desc}</p>
                            <div className="card-actions flex justify-around">
                                <section>
                                    <p><strong>Price:</strong>  {product.price}</p>
                                    <p><strong>Discount:</strong>  {product.discount}</p>
                                </section>
                                <button className="btn btn-md btn-warning rounded-xl">Add to Cart</button>
                            </div>
                        </div>
                    </div>
                </div>

            ))}
        </div>
    );
}
