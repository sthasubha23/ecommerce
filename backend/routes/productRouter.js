const express = require('express')
const { getProduct, getSingleProduct, postProduct, deleteProduct, updateProduct } = require('../controller/productController')
const router = express.Router()

router.get('/', getProduct)
router.get('/:id', getSingleProduct)
router.post('/', postProduct)
router.delete('/:id', deleteProduct)
router.patch('/:id', updateProduct)

module.exports = router