
import React, { useState } from 'react';

const AddProduct = () => {
  const [formData, setFormData] = useState({
    name: '',
    desc: '',
    price: '',
    quantity: '',
    merchant_id: '',
    discount_id: '',
    category_id: '',
    // review_id: '',
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    // Handle form submission here
    console.log('Form data submitted:', formData);
  };

  return (
    <div className="container mx-auto mt-8">
      <form onSubmit={handleSubmit} className="max-w-md mx-auto">
        <label className="block mb-2 text-gray-800">Name</label>
        <input
          type="text"
          name="name"
          value={formData.name}
          onChange={handleChange}
          className="w-full p-2 mb-4 border rounded"
        />

        <label className="block mb-2 text-gray-800">Description</label>
        <textarea
          name="desc"
          value={formData.desc}
          onChange={handleChange}
          className="w-full p-2 mb-4 border rounded"
        ></textarea>

        <label className="block mb-2 text-gray-800">Price</label>
        <input
          type="text"
          name="price"
          value={formData.price}
          onChange={handleChange}
          className="w-full p-2 mb-4 border rounded"
        />

        <label className="block mb-2 text-gray-800">Quantity</label>
        <input
          type="text"
          name="quantity"
          value={formData.quantity}
          onChange={handleChange}
          className="w-full p-2 mb-4 border rounded"
        />

        <label className="block mb-2 text-gray-800">Merchant ID</label>
        <input
          type="text"
          name="merchant_id"
          value={formData.merchant_id}
          onChange={handleChange}
          className="w-full p-2 mb-4 border rounded"
        />

        <label className="block mb-2 text-gray-800">Discount *in percentage*</label>
        <input
          type="text"
          name="discount_id"
          value={formData.discount_id}
          onChange={handleChange}
          className="w-full p-2 mb-4 border rounded"
        />

        <label className="block mb-2 text-gray-800">Category</label>
        <input
          type="text"
          name="category_id"
          value={formData.category_id}
          onChange={handleChange}
          className="w-full p-2 mb-4 border rounded"
        />

        {/* <label className="block mb-2 text-gray-800">Review ID</label>
        <input
          type="text"
          name="review_id"
          value={formData.review_id}
          onChange={handleChange}
          className="w-full p-2 mb-4 border rounded"
        /> */}

        <button
          type="submit"
          className="w-full p-2 text-white bg-blue-500 rounded hover:bg-blue-600"
        >
          Submit
        </button>
      </form>
    </div>
  );
};

export default AddProduct;
