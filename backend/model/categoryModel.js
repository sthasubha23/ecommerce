const { default: mongoose, model } = require("mongoose");
const Schema = mongoose.Schema

const categorySchema = new Schema(
    {
        name: {
            type: String,
            require: true
        },
        product_id :{
            type : mongoose.Schema.Types.ObjectId,
            ref: 'Product',
        }
    },
    { timestamps: true }
)


module.exports = mongoose.model('Category', categorySchema)
