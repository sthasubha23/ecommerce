const { default: mongoose, model } = require("mongoose");
const Schema = mongoose.Schema

const productSchema = new Schema(
    {
        name: {
            type: String,
            require: true
        },
        desc: {
            type: String,
            require: true
        },
        price: {
            type: Number,
            require: true
        },
        quantity: {
            type: Number,
            require: true
        }, 
        merchant_id : {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Merchant'
        },
        discount : {
            name: {
                type: String,
                require: true
            },
            active : {
                type: Boolean,
                require: true
            },
            discount_percentage : {
                type : Number,
                require : true
            }
        },
        category_id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Category",
        },
        review_ids: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Review',
          }],
        image: {
            type: String,
            require: true
        }
    },
    { timestamps: true }
)

module.exports = mongoose.model('Product', productSchema)
