const { default: mongoose } = require("mongoose");
const Schema = mongoose.Schema

const merchantModel = new Schema(
    {
        username: {
            type: String,
            require: true
        },
        password: {
            type: String,
            require: true
        },
        number : {
            type : Number,
            require : true
        },
        address : {
            type : String,
            require :true
        },
        email : {
            type : String,
            require : true
        },
        payment_id : {
            type : mongoose.Schema.Types.ObjectId,
            ref: 'Payment',
        },
        order_id : {
            type : mongoose.Schema.Types.ObjectId,
            ref: 'Order',
        },
        product_id : {
            type : mongoose.Schema.Types.ObjectId,
            ref: 'Product',
        }
    },
    { timestamps: true }
)

module.exports = mongoose.model('Merchant', merchantModel)