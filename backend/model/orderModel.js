const { default: mongoose, model } = require("mongoose");
const Schema = mongoose.Schema

const orderSchema = new Schema(
    {
        product_id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Product',
        },
        user_id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
        },
        quantity: {
            type: Number,
            require: true,
        },
        amount : {
            type : String,
            require : true
        }
    },
    { timestamps: true }
)


module.exports = mongoose.model('Order', orderSchema)
